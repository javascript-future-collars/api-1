const button = document.createElement("button");
button.id = "searchKitty";
button.innerText = "Fetch Kitty";
document.body.appendChild(button);

const showKitty = (url) => {
  const img = document.createElement("img");
  img.setAttribute("data-test", "img-kitty");
  img.src = url;
  document.body.appendChild(img);
};

const fetchKitty = () => {
  const url = "https://api.thecatapi.com/v1/images/search";
  fetch(url)
    .then((response) => response.json())
    .then((data) => showKitty(data[0].url));
};

button.addEventListener("click", fetchKitty);
